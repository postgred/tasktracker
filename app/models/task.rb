class Task < ActiveRecord::Base
  belongs_to :project
  belongs_to :user
  acts_as_taggable
  acts_as_taggable_on :tags

  state_machine :initial => :new do
  
    event :checked do
      transition :new => :work
      transition :work => :verification
      transition :verification => :done
      transition :done => :using
    end

    state :new do
    end

    state :work do
    end

    state :verification do
    end

    state :done do
    end

    state :using do
    end
  end
end
