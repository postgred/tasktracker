class AdminController < ApplicationController
  before_action :authenticate_admin!
  def new
    @admin = User.new
  end

  def create
    @admin = User.create!(user_params)
    @admin.save
    redirect_to root_path
  end

  private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end
end
