json.array!(@tasks) do |task|
  json.extract! task, :id, :title, :rating, :description
  json.url task_url(task, format: :json)
end
