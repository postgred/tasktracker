module ApplicationHelper

  def authenticate_admin!
    if current_user.status != true then
      redirect_to root_path
    end
  end

  def authenticate_admin?
    if current_user.status == true then
      return true
    else
      return false
    end
  end
end
