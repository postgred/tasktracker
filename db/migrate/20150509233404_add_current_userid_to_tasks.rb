class AddCurrentUseridToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :current_user_id, :integer
  end
end
